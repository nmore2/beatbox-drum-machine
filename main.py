import pygame
from pygame import mixer
pygame.init()

black = (0, 0, 0)
white = (255, 255, 255)
gray = (128, 128, 128)
dark_gray = (50, 50, 50)
light_gray = (170, 170, 170)
blue = (0, 255, 255)
red = (255, 0, 0)
green = (0, 255, 0)
gold = (212, 175, 55)
WIDTH = 1100
HEIGHT = 600

timer = pygame.time.Clock()
fps = 60
beats = 8
bpm = 240
instruments = 5
playing = True
active_length = 0
active_beat = 0
beat_changed = True




# sounds
hi_hat = mixer.Sound('sounds\hi hat.wav')
snare = mixer.Sound('sounds\snare.wav')
kick = mixer.Sound('sounds\kick.wav')
crash = mixer.Sound('sounds\crash.wav')
clap = mixer.Sound('sounds\clap.wav')


pygame.mixer.set_num_channels(instruments * 3)

screen = pygame.display.set_mode([WIDTH, HEIGHT])
pygame.display.set_caption('Beatbox Drum Machine')
label_font = pygame.font.Font('montserrat.regular.ttf', 16)
medium_font = pygame.font.Font('montserrat.regular.ttf', 14)
large_font = pygame.font.Font('montserrat.regular.ttf', 20)




boxes = []
clicked = [[-1 for _ in range(beats)] for _ in range(instruments)]


def play_notes():
    for i in range(len(clicked)):
        if clicked[i][active_beat] == 1:
            if i == 0:
                hi_hat.play()
            if i == 1:
                snare.play()
            if i == 2:
                kick.play()
            if i == 3:
                crash.play()
            if i == 4:
                clap.play()


def draw_grid(clicks, beat):
    boxes = []
    left_box = pygame.draw.rect(screen, gray, [0, 0, 150, HEIGHT - 98], 2)
    bottom_box = pygame.draw.rect(screen, gray, [0, HEIGHT - 100, WIDTH, 100], 2)

    for i in range(instruments):
        pygame.draw.line(screen, gray, (0, i * 100), (147, i * 100), 2)

    hi_hat_text = label_font.render('Hi Hat', True, white)
    screen.blit(hi_hat_text, (15, 40))
    snare_text = label_font.render('Snare', True, white)
    screen.blit(snare_text, (15, 140))
    kick_text = label_font.render('Kick', True, white)
    screen.blit(kick_text, (15, 240))
    crash_text = label_font.render('Crash', True, white)
    screen.blit(crash_text, (15, 340))
    clap_text = label_font.render('Clap', True, white)
    screen.blit(clap_text, (15, 440))
    
    for i in range(beats):
        for j in range(instruments):
            if clicks[j][i] == -1:
                color = gray
            else:
                color = green
                
            rect = pygame.draw.rect(screen, color,
                                    [i * ((WIDTH - 150) // beats) + 160, (j * 100) + 5, ((WIDTH - 200) // beats) - 10,
                                     90], 0, 3)
            boxes.append((rect, (i, j)))
        active = pygame.draw.rect(screen, blue,
                                    [beat * ((WIDTH - 150) // beats) + 160, 0, ((WIDTH - 200) // beats)-10, instruments * 100], 3, 3)

    return boxes






run = True
while run:
    timer.tick(fps)
    screen.fill(black)

    boxes = draw_grid(clicked, active_beat)
    
    # drawing lower menu
    
    play_pause = pygame.draw.rect(screen, dark_gray, [405, HEIGHT - 85, 110, 70], 0, 5)
    play_text = label_font.render('Play/Pause', True, white)
    screen.blit(play_text, (415, HEIGHT - 60))

    
    # bpm
    bpm_rect = pygame.draw.rect(screen, dark_gray, [550, HEIGHT - 85, 140, 70], 2)
    bpm_text = medium_font.render('Beats per Minute', True, white)
    screen.blit(bpm_text, (560, HEIGHT - 70))
    bpm_text2 = label_font.render(f'{bpm}', True, white)
    screen.blit(bpm_text2, (560, HEIGHT - 50))
    bpm_add_rect = pygame.draw.rect(screen, dark_gray, [700, HEIGHT - 85, 28, 28], 2)
    bpm_sub_rect = pygame.draw.rect(screen, dark_gray, [700, HEIGHT - 45, 28, 28], 2)
    add_text = medium_font.render('+5', True, white)
    screen.blit(add_text, (705, HEIGHT - 80))
    sub_text = medium_font.render('-5', True, white)
    screen.blit(sub_text, (705, HEIGHT - 40))

    colors = [gray, white, gray]

    if beat_changed:
        play_notes()
        beat_changed = False
    

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False
        if event.type == pygame.MOUSEBUTTONDOWN:
            for i in range(len(boxes)):
                if boxes[i][0].collidepoint(event.pos):
                    coords = boxes[i][1]
                    clicked[coords[1]][coords[0]] *= -1
    
        if event.type == pygame.MOUSEBUTTONUP:
            if play_pause.collidepoint(event.pos) and playing:
                playing = False
            elif play_pause.collidepoint(event.pos) and not playing:
                playing = True
            if bpm_add_rect.collidepoint(event.pos):
                bpm += 5
            elif bpm_sub_rect.collidepoint(event.pos):
                bpm -= 5
                
        
    
    beat_length = 3600 // bpm

    if playing:
        if active_length < beat_length:
            active_length += 1
        else:
            active_length = 0
            if active_beat < beats - 1:
                active_beat += 1
                beat_changed = True
            else:
                active_beat = 0
                beat_changed = True

    pygame.display.flip()


pygame.quit()