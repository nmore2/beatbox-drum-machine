import pytest
from main import *

# Test case for verifying initial state of the Beatbox Drum Machine
def test_initial_state():
    assert bpm == 240
    assert len(clicked) == instruments
    assert len(clicked[0]) == beats

# Test case for verifying sound playback
def test_sound_playback():
    play_notes()

# Test case for verifying BPM adjustment
def test_bpm_adjustment():
    global bpm
    initial_bpm = bpm
    bpm += 5
    assert bpm == initial_bpm + 5
    bpm -= 10
    assert bpm == initial_bpm - 5

# Test case for verifying grid interaction
def test_grid_interaction():
    global clicked
    initial_state = clicked[0][0]
    clicked[0][0] *= -1
    assert clicked[0][0] != initial_state

