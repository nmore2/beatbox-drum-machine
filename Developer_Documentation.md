# Developer Documentation

## Project Structure
The project consists of the following files and directories:

- **main.py:** This file contains the main program logic for the Beatbox Drum Machine.
- **sounds/:** This directory holds the sound files (.wav) used for the drum machine's instruments.
- **montserrat.regular.ttf:** This file is a font used for rendering text in the user interface.

## Dependencies
The project requires Python and the Pygame library. Ensure you have Python installed on your system, and you can install Pygame using pip:

```bash
pip install pygame
```

## Running the Project
To run the Beatbox Drum Machine:

- Clone or download the project repository.
- Navigate to the project directory.
- Ensure the required sound files are present in the sounds/ directory.
- Run the main.py script using Python:

```bash
python main.py
```

## Modifying the Code
Feel free to modify the main.py file to add new features, adjust the user interface, or enhance functionality. The code is well-commented for easier understanding and modification.