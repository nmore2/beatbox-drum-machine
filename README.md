# Beatbox Drum Machine

## Description
This project is a simple drum machine implemented using Pygame library in Python. It allows users to create drum patterns by clicking on grid cells corresponding to different instruments and play them back in real-time. The drum machine provides five different instruments: Hi Hat, Snare, Kick, Crash, and Clap, each mapped to a row in the grid. Users can adjust the beats per minute (BPM) to control the tempo of the pattern.

## Building and Running the Project
1. Install Python (if not already installed) and ensure Pygame library is installed.
2. Clone or download the project repository.
3. Ensure the sound files (hi hat.wav, snare.wav, kick.wav, crash.wav, clap.wav) are present in a subdirectory named 'sounds'.
4. Install the required packages using the following command:

   `pip install -r requirements.txt`
5. Run the Python script `python main.py`.

## Testing
Testing of the project was primarily manual, ensuring that clicking on grid cells toggles instrument presence correctly, playback works as expected, and BPM adjustment changes the tempo accurately. Various BPM settings and instrument combinations were tested to verify the reliability of the drum machine.

## Example
Suppose you want to create a simple drum pattern with a BPM of 120, consisting of a Hi Hat on beats 1 and 3, a Snare on beat 2, and a Kick on beat 4. You would follow these steps:
1. Set the BPM to 200.
2. Click on the grid cells corresponding to the Hi Hat on beats 1 and 3, the Snare on beat 2, and the Kick on beat 4.
3. Click the "Play/Pause" button to start playback.

## Reflection
The project successfully implements a basic drum machine interface with playback functionality. It provides a simple and intuitive way to create drum patterns. However, there are areas for improvement, such as adding more features like pattern saving/loading, expanding instrument options, and enhancing the user interface for better usability. Overall, I'm satisfied with the result as it achieves the core functionality, but there's room for further development to make it more versatile and user-friendly.

## License
This project is licensed under the [MIT License](https://gitlab.com/nmore2/beatbox-drum-machine/-/blob/main/LICENSE.txt?ref_type=heads). If the file header only contains a copyright header (e.g., "Copyright (c) Microsoft Corporation. All rights reserved.", you can assume the associated file to be MIT-licensed